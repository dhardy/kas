KAS RGX
======

Toolkit rendering over the [rgx](https://crates.io/crates/rgx) lib.


Copyright and Licence
-------

The [COPYRIGHT](../COPYRIGHT) file from the main KAS library includes a list of
contributors who claim copyright on this project. This list may be incomplete;
new contributors may optionally add themselves to this list.

The KAS RGX library is published under the terms of the Apache License, Version 2.0.
You may obtain a copy of this licence from the <LICENSE-MIT> file or on
the following webpage: <https://opensource.org/licenses/MIT>
